<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Celsius2Fahrenheit</title>
  </head>
  <body>
    <h2>Conversão para unidade de temperatura</h2>
    <h4>Selecione a unidade de entrada</h4>
    <form action="temperatura" method="get">
      <select name="select-temperatura">
        <option value="celsius">Celsius</option>
        <option value="fahrenheit">Fahrenheit</option>
      </select>
      <h4>Entre com o valor da temperatura</h4>
      <input name="input-value" value="00.0" type="text">
      <input name="converter" type="submit" value="Converter">
    </form>
  </body>
</html>
