package org.ita.temperatura;

/**
 * Unidade internacional de temperatura
 */
public enum UnidadeTemperatura {

    /**
     * Unidade para Fahrenheit
     */
    Fahrenheit("ºF"),
    /**
     * Unidade para Celsius
     */
    Celsius("ºC");

    /**
     * Cache
     */
    private final String unidade;

    /**
     * Preeche valor de temperatura
     * @param unidade Unidade para temperatura
     */
    private UnidadeTemperatura(final String unidade) {
        this.unidade = unidade;
    }

    @Override
    public String toString() {
        return this.unidade;
    }
}
