package org.ita.temperatura;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Converte temperatura Servlet
 */
@WebServlet(name="/temperatura")
public class Celsius2Fahrenheit extends HttpServlet {
    /**
     * Converte unidade em Celsius para Fahrenheit
     * @param celsius Valor da temperatura em Celsius
     * @return Valor da temperatura em Fahrenheit
     */
    private Double celsius2fahrenheit(final Double celsius) {
        return ((celsius * 9) / 5) + 32;
    }

    /**
     * Converte unidade em Fahrenheit para Celsius
         * @param fahrenheit Valor da temperatura em Fahrenheit
     * @return Valor da temperatura em Celsius
     */
    private Double fahrenheit2celsius(final Double fahrenheit) {
        return ((fahrenheit - 32) * 5) / 9;
    }

    /**
     * Envia valor de saida para página
     * @param rp Resposta
     *
     * @param temperatura Temperatura final
     */
    private void imprimeResposta(HttpServletResponse rp, final Double temperatura, final UnidadeTemperatura unidade) throws IOException {
        rp.setContentType("text/html");
        rp.setCharacterEncoding("UTF-8");
        PrintWriter out = rp.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<head><title>Celsius2Fahrenheit</title></head>");
        out.println("<body>");
        out.println("<h3 id=resultado>Resultado: " + String.format("%.2f", temperatura) + " " + unidade +"</h3>");
        out.println("</body>");
    }

    /**
     * Trata recebimento de GET
     * @param rq request HTTP
     * @param rp respnse HTTP
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest rq, HttpServletResponse rp) throws ServletException, IOException {
        Double temperatura = new Double(rq.getParameter("input-value"));
        String option = rq.getParameter("select-temperatura");

        if (option.equals("celsius")) {
            imprimeResposta(rp, celsius2fahrenheit(temperatura), UnidadeTemperatura.Fahrenheit);
        } else {
            imprimeResposta(rp, fahrenheit2celsius(temperatura), UnidadeTemperatura.Celsius);
        }
    }
}
